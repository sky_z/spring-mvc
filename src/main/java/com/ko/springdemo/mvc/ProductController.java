package com.ko.springdemo.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/product")
public class ProductController {

	// method for showing the form
	@RequestMapping("/showForm")
	public String showForm(Model theModel) {

		// create a new product object
		Product theProduct = new Product();

		// add product object to the model
		theModel.addAttribute("product", theProduct);

		return "product-form";
	}

	@RequestMapping("/processForm")
	public String processForm(@ModelAttribute("product") Product theProduct) {

		// log the input data
		System.out
				.println("theProduct: " + theProduct.getName() + " " + "in the category: " + theProduct.getCategory());

		return "product-confirmation";
	}

}
