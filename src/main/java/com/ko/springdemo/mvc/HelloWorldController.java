package com.ko.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

	// need a controller method for showing the initial HTML form

	@RequestMapping("/showForm")
	public String showForm() {
		return "helloworld-form";
	}

	// need a controller method to process the HTML form
	@RequestMapping("/processForm")
	public String processForm() {
		return "helloworld";
	}

	// new controller method for reading form data and add to model
	@RequestMapping("/processFormVersionTwo")
	public String correctTheName(HttpServletRequest request, Model model) {

		// read the request parameter from the HTML form
		String theName = request.getParameter("productName");

		// convert the data to correct form
		theName = theName.toUpperCase();

		// create the message
		String result = "KO-" + theName;

		// add message to the model
		model.addAttribute("message", result);
		return "helloworld";
	}

	@RequestMapping("/processFormVersionThree")
	public String correctTheNameThree(@RequestParam("productName") String theName, Model model) {

		// convert the data to correct form
		theName = theName.toUpperCase();

		// create the message
		String result = "Correct Name of the product: KO-" + theName;

		// add message to the model
		model.addAttribute("message", result);
		return "helloworld";
	}

}
