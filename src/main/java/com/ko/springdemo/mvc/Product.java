package com.ko.springdemo.mvc;

import java.util.LinkedHashMap;

public class Product {

	// fields
	private String name;
	private String category;
	private String location;
	private String originCountry;

	private LinkedHashMap<Integer, String> locationOptions;

	private String[] qualityProperties;

	// no-arg constructor
	public Product() {

		// populate location option
		locationOptions = new LinkedHashMap<>();

		locationOptions.put(1200, "Brigettenau");
		locationOptions.put(1220, "Donaustadt");
		locationOptions.put(1100, "Favoriten");
		locationOptions.put(1110, "Simmering");
		locationOptions.put(1020, "Leopoldstadt");
		locationOptions.put(1120, "Meidling");
		locationOptions.put(1150, "Ruedolfsheimfuenfhaus");

	}

	// getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public LinkedHashMap<Integer, String> getLocationOptions() {
		return locationOptions;
	}

	public String[] getQualityProperties() {
		return qualityProperties;
	}

	public void setQualityProperties(String[] qualityProperties) {
		this.qualityProperties = qualityProperties;
	}

}
