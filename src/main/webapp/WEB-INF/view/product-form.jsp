<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<html>
<head></head>

<body>

	<form:form action="processForm" modelAttribute="product">

		Name: <form:input path="name" />

		<br></br>
		 
		Category: <form:input path="category" />

		<br></br>
		
		<form:select path="location">
		
			<form:options items="${product.locationOptions}"></form:options>
			
			
		</form:select>
		
		<br></br>
		
		Origin Country:
		
		Namibia <form:radiobutton path="originCountry" value="Namibia"/>
		Vietnam <form:radiobutton path="originCountry" value="Vietnam"/>
		Japan <form:radiobutton path="originCountry" value="Japan"/>
		Argentina <form:radiobutton path="originCountry" value="Argentina"/>
		
		<br></br>
		
		Quality Properties:
		
		Sustainable <form:checkbox path="qualityProperties" value="Sustainable"/>
		VSD <form:checkbox path="qualityProperties" value="VSD"/>
		Handmade <form:checkbox path="qualityProperties" value="Handmade"/>
		
		<br></br>
		
		<input type="submit" value="Submit"/>

	</form:form>

</body>

</html>