<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<title>Product Confirmation</title>
</head>

<body>

The product is confirmed: ${product.name} ${product.category}

<br></br>

Is located in: ${product.location}

<br></br>

Was made in: ${product.originCountry}

<br></br>

Quality Properties of the product: 

<ul>
	<c:forEach var="temp" items="${product.qualityProperties}">
	
		<li>${temp}</li>
	
	</c:forEach>

</ul>
	
</body>


</html>